---
hide:
  - toc
---

# FORCEWAIT

| 関数名                                                             | 引数 | 戻り値 |
| :----------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`FORCEWAIT`](./FORCEWAIT.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	FORCEWAIT
    ```
	右クリック、マクロのスキップでスキップできない[`WAIT`](./WAIT.md)命令です。
	この命令に達した時点でこれらのスキップ状態は解除されます。

!!! hint "ヒント"

    命令のみ対応しています。
