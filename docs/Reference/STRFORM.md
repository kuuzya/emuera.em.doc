---
hide:
  - toc
---

# STRFORM

| 関数名                                                         | 引数     | 戻り値   |
| :------------------------------------------------------------- | :------- | :------- |
| ![](../assets/images/IconEmuera.webp)[`STRFORM`](./STRFORM.md) | `string` | `string` |

!!! info "API"

    ```  { #language-erbapi }
	string STRFORM formedString
    ```
    与えられた文字列をPRINTFORMなどと同様の書式付文字列とみなし、展開後の文字列を返します。  


!!! hint "ヒント"

    命令、式中関数両方対応しています。
