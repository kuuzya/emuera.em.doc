---
hide:
  - toc
---

# SETBGMVOLUME

| 函数名                                                               | 参数  | 返回值 |
| :------------------------------------------------------------------- | :---- | :----- |
| ![](../assets/images/IconEE.webp)[`SETBGMVOLUME`](./SETBGMVOLUME.md) | `int` | `void` |

!!! info "API"

    ``` { #language-erbapi }
    SETBGMVOLUME int(0～100)
    ```

    将 `PLAYBGM` 的音量设置为 `0` ～ `100` 之间。

!!! hint "提示"

    只能作为命令使用。
