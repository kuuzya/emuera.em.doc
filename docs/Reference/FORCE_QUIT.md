---
hide:
  - toc
---

# FORCE_QUIT

| 関数名                                                           | 引数   | 戻り値 |
| :--------------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`FORCE_QUIT`](./FORCE_QUIT.md) | `void` | `void` |

!!! info "API"

	``` { #language-erbapi }
	FORCE_QUIT
	```

	`WAIT`を挟まずに`QUIT`する

!!! hint "ヒント"

	命令としてのみ使用可能
