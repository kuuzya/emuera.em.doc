---
hide:
  - toc
---

# GETSECOND

| 関数名                                                             | 引数 | 戻り値 |
| :----------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`GETSECOND`](./GETSECOND.md) | なし | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int GETTIME
    ```
	西暦0001年1月1日からの経過時間を秒単位で取得し、RESULT:0に代入します。  
	そのまま加減算ができるので経過時間などを調べるにはGETTIMEよりも適しています。  

!!! hint "ヒント"

    命令、式中関数両方対応しています。
