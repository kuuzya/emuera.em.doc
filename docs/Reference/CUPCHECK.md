---
hide:
  - toc
---

# CUPCHECK

| 関数名                                                           | 引数 | 戻り値 |
| :--------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`CUPCHECK`](./CUPCHECK.md) | `int`| なし   |

!!! info "API"

    ```  { #language-erbapi }
	CUPCHECK charaID
		```
	CUP、CDOWNに対応するUPCHECKであるCUPCHECK追加  
	　書式：CUPCHECK <キャラ>  
	　内容：引数で指定したキャラに対するUPCHECKを走らせる、それだけ  
	当然ですがUP,DOWNの影響はありません。また、UPCHECKでは結果を表示していましたが、CUPCHECKによる結果は表示されません。  

!!! hint "ヒント"

    命令のみ対応しています。
