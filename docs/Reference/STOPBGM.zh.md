---
hide:
  - toc
---

# STOPBGM

| 函数名                                                     | 参数   | 返回值 |
| :--------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`STOPBGM`](./STOPBGM.md) | `void` | `void` |

!!! info "API"

    ``` { #language-erbapi }
    STOPBGM
    ```

    停止 `PLAYBGM` 正在播放的音轨。

!!! hint "提示"

    只能作为命令使用。
