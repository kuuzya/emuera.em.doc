---
hide:
  - toc
---

# FUNCTIONNAME

| 関数名                                                             | 引数 | 戻り値 |
| :----------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/Icon.webp)[`FUNCTIONNAME`](./FUNCTIONNAME.md) | ``   | ``     |

!!! info "API"

    ```  { #language-erbapi }
	Referense
    ```
    Summary


!!! hint "ヒント"

    命令、式中関数両方対応しています。


!!! example "例" 
    
    ``` { #language-erb title="MAIN.ERB" }
    @SYSTEM_TITLE 
		Sample code
    ``` 
    ``` title="結果"

    ```
