---
hide:
  - toc
---

# WAITANYKEY

| 関数名                                                               | 引数 | 戻り値 |
| :------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`WAITANYKEY`](./WAITANYKEY.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	WAITANYKEY
    ```
	いずれかのキー入力、またはマウスのクリックを待つ[`WAIT`](./WAIT.md)命令です。  
	`WAIT`の[`ONEINPUT`](./ONEINPUT.md)版ともいえます。  

!!! hint "ヒント"

    命令のみ両方対応しています。
