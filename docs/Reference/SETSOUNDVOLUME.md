---
hide:
  - toc
---

# SETSOUNDVOLUME

| 関数名                                                                   | 引数  | 戻り値 |
| :----------------------------------------------------------------------- | :---- | :----- |
| ![](../assets/images/IconEE.webp)[`SETSOUNDVOLUME`](./SETSOUNDVOLUME.md) | `int` | `void` |

!!! info "API"

	``` { #language-erbapi }
	SETSOUNDVOLUME int(0～100)
	```

	`PLAYSOUND`の音量を0～100の値で設定する

!!! hint "ヒント"

    命令としてのみ使用可能
